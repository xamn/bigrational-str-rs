use std::str::FromStr;
use crate::*;
use num::BigRational;

#[test]
fn test0() {
    assert_eq!(format!("{}", big_rational_to_string(BigRational::from_str("1/-6").unwrap())), "-0.1(6)");
    assert_eq!(format!("{}", big_rational_to_string(str_to_big_rational("-1.0(54)").unwrap())), "-1.0(54)")
}

#[test]
fn test1() {}

#[test]
fn test2() {
    assert!(str_to_big_rational("-").is_err());
    assert!(str_to_big_rational("--0").is_err());
    assert!(str_to_big_rational("").is_err());
    assert!(str_to_big_rational("283482384.8237408328472").is_ok());
}

#[test]
fn test_from_dec_str() {
    [
        ("0", BigRational::zero()),
        ("1", BigRational::from_usize(1).unwrap()),
        ("283482384.8237408328472", BigRational::new_raw(BigInt::from_str("2834823848237408328472").unwrap(), BigInt::from_str("10000000000000").unwrap())),
        ("-0.1(6)", BigRational::from_str("1/-6").unwrap()),
    ].iter().for_each(|(input, want)| {
        let got = BigRational::from_dec_str(input).unwrap();
        assert_eq!(got, *want);
    })
}